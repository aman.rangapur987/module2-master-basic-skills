### Face Recognition with Python & OpenCV

 We will build using python dlib’s facial recognition network. Dlib is a general-purpose software library. Using dlib toolkit, we can make real-world machine learning applications.

 Python provides face_recognition API which is built through dlib’s face recognition algorithms. This face_recognition API allows us to implement face detection, real-time face tracking and face recognition applications.
 
 ##Install libraries through following commands.
```
pip3 install dlib 
pip3 install face_recognition
```
![](https://projectgurukul.org/wp-content/uploads/2020/05/face-recognition-project-feature-extraction-matching.jpg)

## Overview of OpenFace for a single input image

    Detect faces with a pre-trained models from dlib or OpenCV.
    Transform the face for the neural network. This repository uses dlib's real-time pose estimation with OpenCV's affine transformation to try to make the eyes and bottom lip appear in the same location on each image.
    Use a deep neural network to represent (or embed) the face on a 128-dimensional unit hypersphere. The embedding is a generic representation for anybody's face. Unlike other face representations, this embedding has the nice property that a larger distance between two face embeddings means that the faces are likely not of the same person. This property makes clustering, similarity detection, and classification tasks easier than other face recognition techniques where the Euclidean distance between features is not meaningful.
    Apply clustering or classification techniques to the features to complete the face recognition task.

![](https://raw.githubusercontent.com/aakashjhawar/face-recognition-using-deep-learning/master/images/openface.jpg)

##Steps for Face Recognition.

> Create dataset of face images.

> Place the face images in dataset folder.

> Extract facial embeddings.

> Train the SVM model.

> Test the model.


`For face recognition we use two programs i.e., training the images from camera and saving it using pickle and running the main program for recognizing the images which we saved previously. ` 