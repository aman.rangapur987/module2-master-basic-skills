## Face Detection by OpenCV

![](https://d1m75rqqgidzqn.cloudfront.net/wp-data/2020/07/02173852/shutterstock_680761540-1.jpg)

## OpenCV
OpenCV is one of the most popular and successful libraries for computer vision and it has an immense number of users because of its simplicity, processing time and high demand in computer vision applications. OpenCV-Python is like a python wrapper around the C++ implementation. OpenCv has more than 2500 implemented algorithms which are freely available for commercial purpose as well.

![](https://lh6.googleusercontent.com/x8jyUeosey-IzUcX1HAKF-hHYkVHtZGlwjkaMMeGMyClPQqUWma60HNKW-j3c5QXP6_xifqZgQj5sLu0eUvT83R5YWxJqwdnEQ8DmPc9NDG6JIwYdVY0dGh7IXDlmae38u2Hi6MI)

## Face detection using OpenCV:
![](https://lh3.googleusercontent.com/IdcOyMJ4hCDvSJXWBo1Rxr1BTM9fQWoxShs0tdS93bpyQ1K6vIog_mV9LrfE0DwKK61X2fHY51AAbPJTkOOMDUVxaiE32JsGog74k3lnXKXPefpd_fSC3divPG3AEEQhaith6S47)

## Reading an image using OpenCV:
Machine converts images into an array of pixels where the dimensions of the image depending on the resolution of the image. The computer reads any image as a range of values between 0 and 255. Colour images have 3 primary channels  

    Red, 
    Green 
    and Blue

imread module:
    img=cv2.imread(‘input image’,1) → this 1 stands for color image
    img=cv2.imread(‘input image’,0) → 0 stands for grayscale image

The reason why we need to convert them is that colour increases the complexity of the model. The inherent complexity of gray scale images is lower than coloured images.
 Syntax to resize the image:

           cv2.resize(src, dsize[, dst[, fx[, fy[, interpolation]]]])

  Now you all have doubts, what are those parameters right?

    src → src stands for the source, it converts the original image into numpy array
    dsize→ dsize is basically the desired size of the output image. The nature of the size is tuple.
    fx → fx is used for the scaling factor along X-axis or Horizontal axis
    fy→ fy is also used to scale factor the Y-axis or Vertical axis
    interpolation → Interpolation is a method which decides the pixel value based on the neighbouring pixels and the scale at which the image is being resized
        INTER_NEAREST
        INTER_LINEAR
        INTER_AREA
        INTER_CUBIC
        INTER_LANCZOS4

## Use haarcascade_frontalface_default.xml file to detect face: 
Haar Cascade is basically a machine learning object detection algorithm which is used to identify objects in an image or video.

And also It is a machine learning based approach where a cascade function is trained on a lot of positive and negative images and then it is used to detect objects in other images.

## Detect the face from the image:
`cv2.CascadeClassifier.detectMultiScale(image[, scaleFactor[, minNeighbors[, flags[, minSize[, maxSize]]]]]) `

    image : image stands for matrix of the type CV_8U which contains an image where objects are detected.
    scaleFactor : ScaleFactor helps to find out how much the image size is reduced at each image scale.
    minNeighbors : this parameter helps to detect how many neighbors each candidate rectangle should have to retain it. This parameter has a direct relation with the quality of the detected faces: higher value means less detection with higher quality.
    flags : Flags is used for the parameter with the same meaning for an old cascade as in the function cvHaarDetectObjects. It is not used for a new cascade.
    minSize : minSize stands for minimum possible object size. If Objects are smaller than that are ignored.

    maxSize :And this parameter is used to determine the  Maximum possible object size. If Objects are larger than that are ignored.

## Draw the rectangle around the face:
Draw rectangle by passing avove parameters by using 
`cv2.rectangle(img,(x,y),(x+w, y+h),(255,0,0),2)`

## Output
This image is taken from internet for further understanding.
![](https://lh5.googleusercontent.com/Jwv2J5FZ2Ci9vJmwUHwos4T7Gzn4t58X_Kgp4qd3HF7q1RrYV54TEVdK4eRT1L0Y5eYKh8Z-4Y8wYoljssTZl6C-37Jxyf2OJWiAjkCgjC41Z6IWrSE0lTg1Nkg5JuiOrYXehc0f)
