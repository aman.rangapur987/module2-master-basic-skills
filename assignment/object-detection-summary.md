## Object Detection using OpenCV
![](https://circuitdigest.com/sites/default/files/projectimage_tut/Object-Detection-using-Python-OpenCV.jpg)

Simple object detection techniques use template matching. We will find an object in an image and then we will describe its features. Features are the common attributes of the image such as corners, edges etc. We will also take a look at some common and popular object detection algorithms such as SIFT, SURF, FAST, BREIF & ORB.

### Object detection using SIFT
If it recognizes the object it would mention objet found. In the code the main part is played by the function which is called as SIFT detector, most of the processing is done by this function. Now the SIFT detector basically have two inputs, one is the cropped image and the other is the image template that we previously defined and then it gives us some matches, so matches are basically the number of objects or keypoints which are similar in the cropped image and the target image. Then we define a threshold value for the matches, if the matches value is greater than the threshold, we put image found on our screen with green color of ROI rectangle.

![](https://circuitdigest.com/sites/default/files/inlineimages/u1/Object-Detection-using-SIFT-in-OpenCV.jpg)
![](https://circuitdigest.com/sites/default/files/inlineimages/u1/Object-Found-using-SIFT-in-OpenCV..jpg)

In the below piece of code, you can see SIFT turns colour image to grayscale, find keypoints, uses Flann Matcher for matching te objects, uses K-NN machine learning algorithm for obtaining matches.

    def sift_detector(new_image, image_template):
    # Function that compares input image to template
    # It then returns the number of SIFT matches between them
    image1 = cv2.cvtColor(new_image, cv2.COLOR_BGR2GRAY)
    image2 = image_template

    # Create SIFT detector object
    #sift = cv2.SIFT()
    sift = cv2.xfeatures2d.SIFT_create()
    # Obtain the keypoints and descriptors using SIFT
    keypoints_1, descriptors_1 = sift.detectAndCompute(image1, None)
    keypoints_2, descriptors_2 = sift.detectAndCompute(image2, None)

    # Define parameters for our Flann Matcher
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 3)
    search_params = dict(checks = 100)

    # Create the Flann Matcher object
    flann = cv2.FlannBasedMatcher(index_params, search_params)

    # Obtain matches using K-Nearest Neighbor Method
    # the result 'matchs' is the number of similar matches found in both images
    matches = flann.knnMatch(descriptors_1, descriptors_2, k=2)


![](https://circuitdigest.com/sites/default/files/inlineimages/u1/Flow-Process-for-SIFT-SURF-FAST-BRIEF-ORB-in-OpenCV.png)

Gradient vectors, corner detection, feature matching, Image Stitching, are few important factors which are available in detectors like SIFT which are important factors for object detection.
![](https://circuitdigest.com/sites/default/files/inlineimages/u1/Normalization-using-OpenCV.png)

As seen in the image, change of intensity or change of contrast we get the below values in arrays which is useful for object detection.
