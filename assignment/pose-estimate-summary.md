### What is Human Pose Estimation?

Human Pose Estimation is defined as the problem of localization of human joints (also known as keypoints - elbows, wrists, etc) in images or videos. It is also defined as the search for a specific pose in space of all articulated poses.

![](https://lh6.googleusercontent.com/mBbue8S4QV2QYnHHad_k8UNa-i3GdPtcerSpu2hYUcvKxgyMRIWsvVnT1qHFO0GWOzZO8bXYmuxjU8PSKB5WRzBJH6W0EPKcCZ0tTazpKcz6DGElojKrv3P9HAJRpnsetRflCP7I)

2D Pose Estimation - Estimate a 2D pose (x,y) coordinates for each joint from a RGB image.
3D Pose Estimation - Estimate a 3D pose (x,y,z) coordinates a RGB image.

![](https://nanonets.com/blog/content/images/2019/04/Screen-Shot-2019-04-11-at-5.17.56-PM.png)

Human Pose Estimation has some pretty cool applications and is heavily used in Action recognition, Animation, Gaming, etc. For example, a very popular Deep Learning app HomeCourt uses Pose Estimation to analyse Basketball player movements.
![](https://nanonets.com/blog/content/images/2019/04/human-action-recognition.png)

## Different approaches to 2D Human Pose Estimation
The classical approach to articulated pose estimation is using the pictorial structures framework. The basic idea here is to represent an object by a collection of "parts" arranged in a deformable configuration (not rigid). A "part" is an appearance template which is matched in an image. Springs show the spatial connections between parts. When parts are parameterized by pixel location and orientation, the resulting structure can model articulation which is very relevant in pose estimation.

## Deep Learning based approaches

The classical pipeline has its limitations and Pose estimation has been greatly reshaped by CNNs. With the introduction of “DeepPose” by Toshev et al, research on human pose estimation began to shift from classic approaches to Deep Learning. Most of the recent pose estimation systems have universally adopted ConvNets as their main building block, largely replacing hand-crafted features and graphical models; this strategy has yielded drastic improvements on standard benchmarks.

The input consists of the image I and a representation of the previous output yt−1
. Keep in mind this is an iterative process and the same output is refined over steps.
Input, xt=I⊕g(yt−1)
where I is the image and yt−1 is the previous output

    f(xt)

outputs the correction εt and this added to the current output yt to generate yt+1,
which takes the corrections into account.
g(yt+1)
converts each keypoint in yt+1 into a heatmap channel so they can be stacked to the image I, so as to form the input for the next teration. This process is repeated T times till we get a refined yt+1 and is brought closer to the ground truth by the addition of εt

![](https://lh4.googleusercontent.com/a6_rmwVDv5D29zTm7RbtGXiKmD8TEDBCoEGUOm_lvL5tH9E4ZnZiC0wWtlD7a2XcXJtlrjWzWt0AhfmDHWfYRwJ4j3IYzAu-s1Sp848-Ii34wj1-Htj86xMOZ8LrOsa3N8mgPoMz)

Mathematically,

    ϵt=f(xt)

yt+1=yt+ϵt
xt+1=I⊕g(yt+1)

f() and g() are learnable and f() is a CNN one important point to note is that as the ConvNet f()
takes I⊕g(yt) as inputs, it has the ability to learn features over the joint input-output space, which is pretty cool.

The parameters Θg and Θf are learnt by optimizing the below equation:

    minΘf,Θg∑Tt=1h(ϵt,e(y,yt))

where, εt and e(y,yt) are predicted, and target corrections respectively. The function h is a measure of distance, such as a quadratic loss. T is the number of correction steps taken by the model.

![](https://lh4.googleusercontent.com/_qqpyGbHwEDfhg6frIvyKp-KduhZQd1awOSLQ_gOpWDPUnWd-VlBdq5NDlTyEpyB70PylaEruoxYkFHbreXZDYW8yHsfMaAeAbdR0NZl15V0Cns_pX_0A_dGxFI_JjBTbSD6_HBX)

